#include "GameBoard.h"
#include <iostream>
#include <string>
#include <sstream>


using namespace std;

// Derived Class
class ChessPiece : public GamePiece
{
public:
  using GamePiece::GamePiece ; 

	virtual std::unique_ptr<GamePiece> clone() const override;
};

std::unique_ptr<GamePiece> ChessPiece::clone() const
{
	// Call the copy constructor to copy this instance
	return std::make_unique<ChessPiece>(*this);
}

void processGameBoard(const GameBoard& board)
{
	const std::unique_ptr<GamePiece>& pawn = board.getPieceAt(0, 0);

	// Doesn't compile, since board is const
	//board.setPieceAt(1, 2, make_unique<ChessPiece>());
}

/*
*/

int main()
{
	GameBoard chessBoard(8, 8);
  string pos = "1aR1 1bN1 1cB1 1dQ1 1eK1 1fB1 1gN1 1hR1 2aP1 2bP1 2cP1 2dP1 2eP1 2fP1 2gP1 2hP1 " ;
  pos +=       "8aR2 8bN2 8cB2 8dQ2 8eK2 8fB2 8gN2 8hR2 7aP2 7bP2 7cP2 7dP2 7eP2 7fP2 7gP2 7hP2 " ; 

  for ( size_t i = 0 ; i < pos.size(); i+= 5 ) {
    size_t row = '8' - pos[i] ;
    size_t col = pos[i+1] - 'a' ;
    size_t player = pos[i+3] - '1' ;
    char ch = pos[i+2]; 
    GamePieceID id = GameBoard::getGamePieceID ( ch );;

	  auto piece = std::make_unique<ChessPiece>( id, player  );
	  chessBoard.setPieceAt( row, col, std::move( piece ));
  }


  // construct a stream from the string
  string str = "f2-f4   e7-e5 f4xe5   d7-d6 e5xd6   Bf8xd6 g2-g3   Qd8-g5 Ng1-f3  Qg5xg3+ h2xg3   Bd6xg3#";

  std::stringstream strstr(str);

  // use stream iterators to copy the stream to the vector as whitespace separated strings
  std::istream_iterator<std::string> it(strstr);
  std::istream_iterator<std::string> end;
  std::vector<std::string> results(it, end);

  
  bool white = true ;
  for (const string& s: results ) {
    if ( white ) {
      cout << s << " ";
    } else {
      cout << s << endl;
    }
    chessBoard.moveGamePiece ( s );
    white = !white ;
  }


  cout << chessBoard << endl;

/*
	GameBoard board2;
	board2 = chessBoard;

	processGameBoard(board2);
*/

	return 0;
}
