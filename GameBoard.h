#pragma once

#include <cstddef>
#include <vector>
#include <memory>
#include <string>

using namespace std;

enum class GamePieceID { PAWN, ROOK, KNIGHT, BISHOP, QUEEN, KING, X } ;

class GamePiece
{
public:

	virtual std::unique_ptr<GamePiece> clone() const = 0; // pure virtual function

  GamePiece ()= default; 
  GamePiece ( GamePieceID id, size_t p ):  m_id(id), m_player(p) {}; // constructor 

  inline virtual size_t player () const { return m_player; } ;

  virtual string name () const ;
  
  GamePieceID m_id = GamePieceID::X ; 
  size_t m_player = 0 ;

};

class GameBoard
{
public: 
  friend std::ostream& operator<< (std::ostream& stream, const GameBoard& b) ;
        
	explicit GameBoard(size_t inWidth = kDefaultWidth, size_t inHeight = kDefaultHeight);
	GameBoard(const GameBoard& src); // copy constructor
	virtual ~GameBoard();
	GameBoard& operator=(const GameBoard& rhs); // assignment operator

	// Sets a piece at a location. The GameBoard becomes owner of the piece.
	// inPiece can be nullptr to remove any piece from the given location.
	void setPieceAt(size_t x, size_t y, std::unique_ptr<GamePiece> inPiece);
	std::unique_ptr<GamePiece>& getPieceAt(size_t x, size_t y);
	const std::unique_ptr<GamePiece>& getPieceAt(size_t x, size_t y) const;

	size_t getHeight() const { return mHeight; }
	size_t getWidth() const { return mWidth; }
	static const size_t kDefaultWidth = 10;
	static const size_t kDefaultHeight = 10;
	virtual void display() const;
	virtual void moveGamePiece( const string & ) ;
  static GamePieceID  getGamePieceID (const char& ch) ;

protected:
private:
  void drawLine ( const string& offset ) const ;
  void drawPieceAt (size_t i, size_t j ) const ;
	void copyFrom(const GameBoard& src);
	void initializeCellsContainer();
	std::vector<std::vector<std::unique_ptr<GamePiece>>> mCells;
	size_t mWidth, mHeight;

  const size_t  OFFSET = 20;
  const size_t GRID_WIDTH = 4;

};
