#include "GameBoard.h"
#include <iostream>

using namespace std;


std::ostream& operator<< (std::ostream& stream, const GameBoard& b) {
  b.display();
  return stream ;
}


string GamePiece::name () const {
  const string names[] = {"P","R", "N", "B","Q","K","X"};
  char sym[2] = { ' ', '-' };
  char ch = sym[ m_player ];
  return ch+ names[(size_t) m_id] + ch;
}

GameBoard::GameBoard(size_t inWidth, size_t inHeight)
: mWidth(inWidth) , mHeight(inHeight)
{
	initializeCellsContainer();
}

GameBoard::GameBoard(const GameBoard& src)
{
	copyFrom(src);
}

GameBoard::~GameBoard()
{
	// Nothing to do, the vector will clean up itself.
}

void GameBoard::initializeCellsContainer()
{
	mCells.resize(mWidth);
	for (auto& column : mCells) {
		column.resize(mHeight);
	}
}

void GameBoard::drawLine ( const string& offset ) const {
  #define  ROW_WIDTH (GRID_WIDTH*mWidth)
  string line = std::string( ROW_WIDTH , '-') ;

  for (size_t i=0;i < line.size(); i+= GRID_WIDTH ) {
    line[i] = '+';
  }
  line = offset + line + "+";
  cout << line << endl;
}
void GameBoard::drawPieceAt (size_t i, size_t j ) const  {
  const unique_ptr<GamePiece>&  pt = getPieceAt(i, j) ;
  string s = std::string( GRID_WIDTH -1  , ' ') + "|";

  if ( pt ) {
    size_t i = (GRID_WIDTH-1)/2;
    s[i] = pt->name()[1] ;
    if ( pt->player()==1 ) {
      s[i-1]='-';
      s[i+1]='-';
    }
  }
  cout << s ;
}

void drawColumnIndex (const string& offset, size_t mWidth, size_t GRID_WIDTH ) {
  cout << offset ;
  for (size_t j = 0; j < mWidth; j++) {
    string s = std::string( GRID_WIDTH  , ' ') ;
    s[GRID_WIDTH/2] = 'a'+j;
    cout << s ;
  }
  cout << endl;
}

void GameBoard::display() const
{
  #define OFFSET 30
  string offset = std::string( OFFSET , ' ') ;

  string starter = offset + "|";

  drawColumnIndex (offset, mWidth, GRID_WIDTH ) ;

  drawLine ( offset );
  for (size_t i = 0; i < mHeight; i++) {
    char ch = '8' -i ;
    starter [ OFFSET - 3 ] = ch;
    cout << starter;
    for (size_t j = 0; j < mWidth; j++) {
      drawPieceAt ( i,j);
    }
    cout <<  "  " << ch << endl;
    drawLine ( offset );
  }

  drawColumnIndex (offset, mWidth, GRID_WIDTH ) ;
}


void GameBoard::copyFrom(const GameBoard& src)
{
	mWidth = src.mWidth;
	mHeight = src.mHeight;

	initializeCellsContainer();

	for (size_t i = 0; i < mWidth; i++) {
		for (size_t j = 0; j < mHeight; j++) {
			if (src.mCells[i][j])
				mCells[i][j] = src.mCells[i][j]->clone();
			else
				mCells[i][j].reset();
		}
	}
}

GameBoard& GameBoard::operator=(const GameBoard& rhs)
{
	// check for self-assignment
	if (this == &rhs) {
		return *this;
	}

	// copy the source GameBoard
	copyFrom(rhs);

	return *this;
}

void GameBoard::setPieceAt(size_t x, size_t y, unique_ptr<GamePiece> inPiece)
{
	mCells[x][y] = move(inPiece);
}

unique_ptr<GamePiece>& GameBoard::getPieceAt(size_t x, size_t y)
{
	return mCells[x][y];
}

const unique_ptr<GamePiece>& GameBoard::getPieceAt(size_t x, size_t y) const
{
	return mCells[x][y];
}

GamePieceID  GameBoard::getGamePieceID (const char& ch) {
  GamePieceID id;
    switch (ch) {
      case 'P':
          id = GamePieceID::PAWN ;
        break;
      case 'R':
          id = GamePieceID::ROOK ;
        break;
      case 'N':
          id = GamePieceID::KNIGHT ;
        break;
      case 'B':
          id = GamePieceID::BISHOP ;
        break;
      case 'Q':
          id = GamePieceID::QUEEN ;
        break;
      case 'K':
          id = GamePieceID::KING ;
        break;
      default :
          id = GamePieceID::X ;
    }
  return id;
}
/*
f2-f4   e7-e5
f4xe5   d7-d6
e5xd6   Bf8xd6
g2-g3   Qd8-g5
Ng1-f3  Qg5xg3+
h2xg3   Bd6xg3#
*/
void GameBoard::moveGamePiece (const string& name) 
{
  string s = name ; 
  if ( islower ( name[0] ) ) {
    s = "P" + s; // P: Pawn implicitly 
  };
  GamePieceID pid = getGamePieceID ( s[0] ) ;
  size_t j0 = s[1]-'a';
  size_t i0 = '8' - s[2];
  size_t j1 = s[4]-'a';
  size_t i1 = '8' - s[5];

  if ( s[3]=='x' ) {
    unique_ptr<GamePiece>& q = getPieceAt(i1, j1 );
    cout << "kill player " << q->player() << ": " << q->name() << endl;
    // the following is to prevent  memory leak;
    unique_ptr<GamePiece> temp = move (q);
  }
  if ( s[3]=='x' ) {
    unique_ptr<GamePiece>& q = getPieceAt(i1, j1 );
    // now q is deleted
    cout << "xxx check pointer q " << q.get() << ": " << endl;
    // cout << "kill player " << q->player() << ": " << q->name() << endl;
  }

 unique_ptr<GamePiece>& p = getPieceAt(i0, j0 );
 setPieceAt(i1, j1, move(p));
 setPieceAt(i0, j0, nullptr);

}
